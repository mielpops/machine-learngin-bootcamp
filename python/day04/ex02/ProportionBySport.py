# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ProportionBySport.py                               :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/20 00:52:13 by tony              #+#    #+#              #
#    Updated: 2020/03/20 18:30:12 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import pandas as pd

def proportionBySport(df, year, sport, gender):
    year_attendee = df.loc[df["Year"] == year]
    gender_attendee = year_attendee.loc[df["Sex"] == gender]
    sport_attendee = gender_attendee.loc[df["Sport"] == sport]
    total_prop = gender_attendee["Name"].drop_duplicates().shape[0]
    gender_prop = sport_attendee["Name"].drop_duplicates().shape[0]

    if total_prop == 0:
        return 0.0
    return (gender_prop / total_prop)
