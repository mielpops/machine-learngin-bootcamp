# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    HowManyMedalsByCountry.py                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/20 19:16:31 by tony              #+#    #+#              #
#    Updated: 2020/03/23 16:42:35 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import pandas as pd

def howManyMedalsByCountry(df, country):
    medals_per_year = {}
    team_selector = df["Team"] == country
    team_rows = df.loc[team_selector]

    medals_sel = team_rows["Medal"].isin(["Gold", "Silver", "Bronze"])
    team_rows = team_rows[medals_sel]
    
    for year in team_rows["Year"].drop_duplicates():
        year_team_rows = team_rows.loc[team_rows["Year"] == year]
        gold_medals = 0
        silver_medals = 0
        bronze_medals = 0

        for sport in year_team_rows["Sport"].drop_duplicates():

            medals_per_sport = year_team_rows.loc[year_team_rows["Sport"] == sport]["Medal"]
            
            if medals_per_sport.loc[medals_per_sport == "Gold"].shape[0] != 0:
                gold_medals += 1
            if medals_per_sport.loc[medals_per_sport == "Silver"].shape[0] != 0:
                silver_medals += 1
            if medals_per_sport.loc[medals_per_sport == "Bronze"].shape[0] != 0:
                bronze_medals += 1

        medals_per_year[year] = {
            "G" : gold_medals,
            "S" : silver_medals,
            "B" : bronze_medals
        }
        
    return medals_per_year