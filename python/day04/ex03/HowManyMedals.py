# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    HowManyMedals.py                                   :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/20 18:31:12 by tony              #+#    #+#              #
#    Updated: 2020/03/20 18:56:34 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import pandas as pd

def howManyMedals(df, name):
    medals_by_year = {}
    whois = df.loc[df["Name"] == name]

    for year in whois["Year"]:
        medals = {
            "Gold" : 0,
            "Silver" : 0,
            "Bronze" : 0
        }

        year_medals = whois.loc[whois["Year"] == year]["Medal"]
        
        for medal in year_medals:
            if medals.get(medal) is not None:
                medals[medal] += 1
        medals_by_year[year] = medals

    return medals_by_year