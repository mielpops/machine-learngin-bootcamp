# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    FileLoader.py                                      :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/19 23:57:37 by tony              #+#    #+#              #
#    Updated: 2020/03/20 00:22:03 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import pandas as pd

class FileLoader(object):
    def __init__(self):
        pass

    def load(self, path):
        df = pd.read_csv(path)
        print(f"Loading datasets of dimensions {df.shape[0]}x{df.shape[1]}")
        return df

    def display(self, df, n):
        print(df[:n])