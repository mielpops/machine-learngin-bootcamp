# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    SpatioTemporalData.py                              :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/20 19:00:16 by tony              #+#    #+#              #
#    Updated: 2020/03/20 19:15:50 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import pandas as pd

class SpatioTemporalData(object):
    def __init__(self, data):
        self.data = data

    def when(self, location):
        place = self.data.loc[self.data["City"] == location]
        return list(place["Year"].drop_duplicates())

    def where(self, date):
        dates = self.data.loc[self.data["Year"] == date]
        return list(dates["City"].drop_duplicates())