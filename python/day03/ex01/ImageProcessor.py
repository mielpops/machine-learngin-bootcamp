# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ImageProcessor.py                                  :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/16 11:54:05 by bbellavi          #+#    #+#              #
#    Updated: 2020/03/16 22:42:01 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import matplotlib.image as mpimg
import matplotlib.pyplot as plt

class ImageProcessor(object):
	def __init__(self):
		pass

	def load(self, path):
		img = mpimg.imread(path)
		height, width, _ = img.shape
		print(f"Loading image of dimensions {width}x{height}")
		return img

	def display(self, array):
		plt.imshow(array)
		plt.show()