# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    NumpyCreator.py                                    :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bbellavi <bbellavi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/16 11:34:05 by bbellavi          #+#    #+#              #
#    Updated: 2020/03/16 11:51:44 by bbellavi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import numpy as np

class NumpyCreator(object):
	def __init__(self):
		pass

	def from_list(self, lst):
		return np.array(lst)

	def from_tuple(self, tpl):
		return np.array(list(tpl))

	def from_iterable(self, itr):
		return np.array(itr)

	def from_shape(self, shape, value=0):
		return np.full(shape, value)

	def random(self, shape):
		return np.random.random_sample(shape)

	def identity(self, n):
		return np.identity(n)