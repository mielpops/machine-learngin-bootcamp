# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ColorFilter.py                                     :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/17 01:50:05 by tony              #+#    #+#              #
#    Updated: 2020/03/18 22:31:09 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import numpy as np

class ColorFilter(object):
    def __init__(self):
        self.thresholds = None

    def invert(self, array):
        """
            Takes a NumPy array of an image as an argument and returns 
            an array with inverted color.

            Authorized function: None
            Authorized operator: -
        """
        new = np.copy(array)
        return 1.0 - new

    def to_blue(self, array):
        """
            Takes a NumPy array of an image as an arument and returns
            an array with a blue filter.

            Authorized function: .zeros, .shape
            Authorized operator: None
        """
        new = np.copy(array)
        new[::,::,:2:] = np.zeros(new[::,::,:2:].shape)
        return new

    def to_green(self, array):
        """
            Takes a NumPy array of an image as an arument and returns
            an array with a green filter.

            Authorized function: None
            Authorized operator: *
        """
        new = np.copy(array)
        new[:,:,::2] = new[:,:,::2] * 0
        return new

    def to_red(self, array):
        """
            Takes a NumPy array of an image as an arument and returns
            an array with a red filter.

            Authorized function: green, blue
            Authorized operator: -, +
        """
        new = np.copy(array)
        blue = new[:,:,2]
        green = new[:,:,1]
        blue -= blue
        green -= green
        return new

def _filter_pixel(self, pixel):
    """
        Takes a pixel and do the average in the threshold range.
    """
    result = None
    thresholds = self.thresholds

    if thresholds is None:
        thresholds = np.linspace(0, 255, 4)

    pixel = pixel * 255
    if pixel < thresholds[0]:
        result = thresholds[0] // 2
    elif pixel >= thresholds[0] and pixel <= thresholds[1]:
        result = thresholds[0] + (thresholds[1] - thresholds[0]) // 2
    elif pixel >= thresholds[1] and pixel <= thresholds[2]:
        result = thresholds[1] + (thresholds[2] - thresholds[1]) // 2
    else:
        result = thresholds[2] + (255 - thresholds[2]) // 2

    return (result * 100) // 255 / 100

def celluloid(self, array, thresholds=None):
    """
        Takes a NumPy array of an image as an argument, and returns
        an array with celluloid shade filter.
        The celluloid filter must display at least four thresholds 
        of shades. Be careful! You are not asked to apply back contour
        on the object here (you will have to, but later ...), you only
        have to work on the shades of your images.

        Authorized function: arange, linspace

        linspace : create evenly spaced array between start and end.
    """
    new = np.copy(array)
    self.thresholds = thresholds
    pfilter = np.vectorize(self._filter_pixel)

    if thresholds is not None:
        if len(thresholds) != 3:
            raise ValueError("Thresholds tuples must be of length 3.")

    new[::,::,0] = pfilter(new[::,::,0])
    new[::,::,1] = pfilter(new[::,::,1])
    new[::,::,2] = pfilter(new[::,::,2])
    return new